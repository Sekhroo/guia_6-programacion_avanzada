import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class main {
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		//Generar la coleccion hastset
		HashSet<String> estudiantes = new HashSet<>();
		HashSet<String> estudiantes2020 = new HashSet<>();
		
		//Explicación del uso de busqueda: En este programa esta coleccion hastset
		//se utilizara como un intermediario entre las operaciones de estudiates y estudiantes2020
		//mas que nada para no modificar estos ultimos. 
		HashSet<String> busqueda = new HashSet<>();
		
		//Establecer los estudiantes
		estudiantes.add("Felipe");
		estudiantes.add("Juliana");
		estudiantes.add("Juan");
		estudiantes.add("Pedro");
		estudiantes.add("Valentina");
		
		//Establecer los estudiantes de la generacion 2020
		estudiantes2020.add("Vicente");
		estudiantes2020.add("Felipe");
		estudiantes2020.add("Juliana");
		estudiantes2020.add("Juan");
		estudiantes2020.add("Pedro");
		estudiantes2020.add("Valentina");
		estudiantes2020.add("Paris");
		estudiantes2020.add("Brandon");
		estudiantes2020.add("Carla");
		
		//Imprimir todo los estudiantes
		System.out.println("Lista de estudiantes: ");
		Iterator<String> iterate = estudiantes.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		
		//-----------------------------------------------------------------------------
		System.out.println("\n");
		System.out.println("\n");
		//-----------------------------------------------------------------------------
		
		//Nombre especifico
		System.out.println("Buscar nombre especifico entre estudiantes: ");
		String nombre = sc.nextLine();
		busqueda.add(nombre);
		if(estudiantes.containsAll(busqueda)) {
			System.out.println("Se encuentra ese estudiante");
		}else {
			System.out.println("No se encuentra ese estudiante");
		}
		busqueda.remove(nombre);
		
		//-----------------------------------------------------------------------------
		System.out.println("\n");
		System.out.println("\n");
		//-----------------------------------------------------------------------------
				
		//Verificar subconjunto
		System.out.println("Estudiantes es un subconjuntos de estudiantes2020: ");
		if(estudiantes2020.containsAll(estudiantes)) {
			System.out.println("Sí");
		}else {
			System.out.println("No");
		}
		
		//-----------------------------------------------------------------------------
		System.out.println("\n");
		System.out.println("\n");
		//-----------------------------------------------------------------------------
		
		//Union de estudiantes
		System.out.println("Unir la lista de estudiantes2020 y estudiantes: ");
		busqueda.addAll(estudiantes2020);
		busqueda.addAll(estudiantes);
		iterate = busqueda.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		busqueda.removeAll(estudiantes2020);
		busqueda.removeAll(estudiantes);
		
		//-----------------------------------------------------------------------------
		System.out.println("\n");
		System.out.println("\n");
		//-----------------------------------------------------------------------------
		
		//Remover estudiantes que no sean parte de estudiantes en estudiantes2020
		System.out.println("Remover estudiantes de la generación 2020 que no se encuentren en estudiantes: ");
		busqueda.addAll(estudiantes2020);
		busqueda.removeAll(estudiantes);
		iterate = busqueda.iterator();
		while(iterate.hasNext()) {
			estudiantes2020.remove(iterate.next());
		}
		iterate = estudiantes2020.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
	}
}
