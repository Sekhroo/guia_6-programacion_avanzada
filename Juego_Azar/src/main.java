import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;

public class main {
	
	public static void main(String[] args) {
		int i = 0;
		int num = 0;
		HashSet<Integer> jugador1 = new HashSet<>();
		HashSet<Integer> jugador2 = new HashSet<>();
		HashSet<Integer> verificador = new HashSet<>();
		Iterator<Integer> iterate = jugador1.iterator();
		
		//Generar numeros de jugador 1
		while(i != 100) {
			num = azar();
			jugador1.add(num);
			iterate = jugador1.iterator();
			while(iterate.hasNext()) {
				iterate.next();
				i++;
			}
			if (i == 7) {
				break;
			}else {
				i = 1;
			}
		}
		//Generar numeros de jugador 2
		while(i != 100) {
			num = azar();
			jugador2.add(num);
			iterate = jugador2.iterator();
			while(iterate.hasNext()) {
				iterate.next();
				i++;
			}
			if (i == 7) {
				break;
			}else {
				i = 1;
			}
		}
		
		while(!verificador.containsAll(jugador1) && !verificador.containsAll(jugador2)) {
			num = azar();
			jugador1.remove(num);
			jugador2.remove(num);
		}
		
		if (verificador.containsAll(jugador1)) {
			System.out.println("Ha ganado el jugador 1");
		}else if(verificador.containsAll(jugador2)){
			System.out.println("Ha ganado el jugador 2");
		}
	}
	
	public static int azar() {
		int num = (int) (Math.random()*10+1);
        return num;
	}
}
